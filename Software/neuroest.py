from tkinter import *
from tkinter.scrolledtext import ScrolledText
from PIL import Image, ImageTk
import time
import serial
import serial.tools.list_ports
import datetime
import tkinter.font as font

global i
global n_items_basal
global height_items_basal
global height_items_basal_bi
global n_items_tbs_bi
global height_items_tbs_bi
global n_items_tbs
global height_items_tbs
global n_items_pbs
global height_items_pbs


#Define janela de tela cheia
root = Tk()																	
root.state('zoomed')	
root.title('Neuroestimulador - Gerenciador de protocolos')													
root.resizable(False, False)	

canvas_big = Canvas(root, bg='LightCyan4')
canvas_big.place(relx=0, rely=0, relwidth=1, relheight=1)												

#--------------------------------------------------Tela 1 (lateral esquerda)-------------------------------------------------------
canvas1 = Canvas(canvas_big, bg='LightSteelBlue1')											
canvas1.place(relx=0.025, rely=0.275, relwidth=0.3, relheight=0.7)					

#Seleção de protocolo:

prot_text1 = Label(canvas1, text="Selecione o primeiro protocolo:", bg='LightSteelBlue1')		
prot_text1.place(relx=0, rely=0, relwidth=1, relheight=0.075)

protocol_list1 = Listbox(canvas1)											
protocol_list1.configure(justify=CENTER)
protocol_list1.insert(1,"Basal unipolar")	
protocol_list1.insert(2,"Basal bipolar")
protocol_list1.insert(3,"TBS unipolar")	
protocol_list1.insert(4,"TBS bipolar")	
protocol_list1.insert(5,"PBS")						
protocol_list1.place(relx=0.1, rely=0.075, relwidth=0.5, relheight=0.165)

button_prot_1 = Button(canvas1, text="Selecionar", command = lambda:select_protocol(canvas1, protocol_list1.get(ACTIVE)))		
button_prot_1.place(relx=0.6, rely=0.075, relwidth=0.3, relheight=0.165)	

#--------------------------------------------------Tela 2 (meio)-------------------------------------------------------
canvas2 = Canvas(canvas_big, bg='LightSteelBlue1')											
canvas2.place(relx=0.35, rely=0.275, relwidth=0.3, relheight=0.7)					

#Seleção de protocolo:

prot_text2 = Label(canvas2, text="Selecione o segundo protocolo:", bg='LightSteelBlue1')		
prot_text2.place(relx=0, rely=0, relwidth=1, relheight=0.075)

protocol_list2 = Listbox(canvas2)											
protocol_list2.configure(justify=CENTER)
protocol_list2.insert(1,"Basal unipolar")	
protocol_list2.insert(2,"Basal bipolar")
protocol_list2.insert(3,"TBS unipolar")	
protocol_list2.insert(4,"TBS bipolar")	
protocol_list2.insert(5,"PBS")							
protocol_list2.place(relx=0.1, rely=0.075, relwidth=0.5, relheight=0.165)

button_prot_2 = Button(canvas2, text="Selecionar", command = lambda:select_protocol(canvas2, protocol_list2.get(ACTIVE)))		
button_prot_2.place(relx=0.6, rely=0.075, relwidth=0.3, relheight=0.165)

#--------------------------------------------------Tela 3 (lateral direita)-------------------------------------------------------

canvas3 = Canvas(canvas_big, bg='LightSteelBlue1')											
canvas3.place(relx=0.675, rely=0.275, relwidth=0.3, relheight=0.7)					

#Seleção de protocolo:

prot_text3 = Label(canvas3, text="Selecione o terceiro protocolo:", bg='LightSteelBlue1')		
prot_text3.place(relx=0, rely=0, relwidth=1, relheight=0.075)

protocol_list3 = Listbox(canvas3)											
protocol_list3.configure(justify=CENTER)
protocol_list3.insert(1,"Basal unipolar")	
protocol_list3.insert(2,"Basal bipolar")
protocol_list3.insert(3,"TBS unipolar")	
protocol_list3.insert(4,"TBS bipolar")	
protocol_list3.insert(5,"PBS")								
protocol_list3.place(relx=0.1, rely=0.075, relwidth=0.5, relheight=0.165)

button_prot_3 = Button(canvas3, text="Selecionar", command = lambda:select_protocol(canvas3, protocol_list3.get(ACTIVE)))		
button_prot_3.place(relx=0.6, rely=0.075, relwidth=0.3, relheight=0.165)

#--------------------------------------------------Tela de avisos (log), ajuda, info e envios ao microcontrolador--------------------------------------------

canvas4 = Canvas(canvas_big, bg='LightCyan2')
canvas4.place(relx=0.025, rely=0.0125, relwidth=0.95, relheight=0.25)
canvaslog = Canvas(canvas4, bg='LightCyan2')
canvaslog.place(relx=0.55, rely=0.075, relwidth=0.42, relheight=0.85)
scroll = Scrollbar(canvaslog)
scroll.pack(side="right",fill="y",expand=False)
log = Text(canvaslog, yscrollcommand=scroll.set)
log.place(relx=0, rely=0, relwidth=0.97, relheight=1)
log.config(state='disabled')

scroll.config(command=log.yview)

con_img = Image.open("img_con.PNG")
con_image = ImageTk.PhotoImage(con_img.resize((20,20)))
play_img = Image.open("img_play.PNG")
play_image = ImageTk.PhotoImage(play_img.resize((20,20)))
stop_img = Image.open("img_stop.PNG")
stop_image = ImageTk.PhotoImage(stop_img.resize((20,20)))
skip_img = Image.open("img_skip.PNG")
skip_image = ImageTk.PhotoImage(skip_img.resize((20,20)))
button_con = Button(canvas4, text="  Estabelecer conexão com Arduino", image=con_image, compound="left", command = lambda:detect_arduino())		
button_con.place(relx=0.33, rely=0.1, relwidth=0.2, relheight=0.15)
button_send = Button(canvas4, text="       Iniciar protocolos selecionados", image=play_image, compound="left", command = lambda:send_protocol())		
button_send.place(relx=0.33, rely=0.3, relwidth=0.2, relheight=0.15)
button_int = Button(canvas4, text="        Parar processo de estimulação", image=stop_image, compound="left",  command = lambda:interrupt_protocol())		
button_int.place(relx=0.33, rely=0.5, relwidth=0.2, relheight=0.15)
button_skip = Button(canvas4, text="     Avançar para próximo protocolo", image=skip_image, compound="left",  command = lambda:jump_protocol())		
button_skip.place(relx=0.33, rely=0.7, relwidth=0.2, relheight=0.15)
est_image = Image.open("neuroest.PNG")
est_photo = ImageTk.PhotoImage(est_image.resize((100,100)))
est_label = Label(canvas4, image=est_photo)
est_label.image = est_image
est_label.place(relx=0.015, rely=0.1, relwidth=0.076, relheight=0.52)

myFont = font.Font(family='Helvetica', size=12, weight='bold')
tit_text = Label(canvas4, text="Neuroestimulador", font = myFont, bg='LightCyan2')		
tit_text.place(relx=0.1, rely=0.1, relwidth=0.15, relheight=0.1)
tit_text = Label(canvas4, text="Gerenciador de protocolos", bg='LightCyan2')		
tit_text.place(relx=0.1, rely=0.2, relwidth=0.15, relheight=0.1)

help_img = Image.open("img_help.PNG")
help_image = ImageTk.PhotoImage(help_img.resize((20,20)))
button_help = Button(canvas4, text="     Ajuda", image=help_image, compound="left",  command = lambda:interrupt_protocol())		
button_help.place(relx=0.1, rely=0.4, relwidth=0.15, relheight=0.15)

ab_img = Image.open("img_ab.PNG")
ab_image = ImageTk.PhotoImage(ab_img.resize((20,20)))
button_ab = Button(canvas4, text="     Sobre", image=ab_image, compound="left",  command = lambda:interrupt_protocol())		
button_ab.place(relx=0.1, rely=0.6, relwidth=0.15, relheight=0.15)


 #Função para escrita no log:

def writeToLog(msg):	
    numlines = log.index('end - 1 line').split('.')[0]
    log['state'] = 'normal'
    if numlines==9:
        log.delete(1.0, 2.0)
    if log.index('end-1c')!='1.0':
        log.insert('end', '\n')
    now = datetime.datetime.now()
    log.insert('end', "[")
    log.insert('end', now.hour)
    log.insert('end', ":")
    log.insert('end', now.minute)
    log.insert('end', "]")
    log.insert('end', " ")
    log.insert('end', msg)
    log['state'] = 'disabled'
    log.see("end")

#Função para definicação de protocolo:		

def select_protocol(canvas, protocol):	
	def InputValidation(S):
		if S.isdigit():
			return True
		else:
			canvas.bell()
			return False
	vcmd = (canvas.register(InputValidation), '%S')
	list = canvas.place_slaves()	
	for l in list:
		if(l!=prot_text1 and l!=prot_text2 and l!=prot_text3 and l!=protocol_list1 and l!=protocol_list2 and l!=protocol_list3 and l!=button_prot_1 and l!=button_prot_2 and l!=button_prot_3):
				l.destroy()									
	if (protocol=="Basal unipolar"):													
		prot_image = Image.open("protocolo_basal.PNG")
		prot_photo = ImageTk.PhotoImage(prot_image.resize((420,110)))
		prot_label = Label(canvas, image=prot_photo)
		prot_label.image = prot_photo 
		prot_label.place(relx=0.0, rely=0.24, relwidth=1, relheight=0.275)				
		n_items_basal = 6
		height_items_basal = 0.5 / n_items_basal
		i=0
		T_text = Label(canvas, text="Intervalo T (us):")
		T_text.place(relx=0, rely=0.5+(i*height_items_basal), relwidth=0.4, relheight=height_items_basal)
		T_entry = Entry(canvas, justify=CENTER, validate='key', vcmd=vcmd)
		T_entry.place(relx=0.4, rely=0.5+(i*height_items_basal), relwidth=0.3, relheight=height_items_basal)
		T_lim = Label(canvas, text="Limites: 5us-1000us")
		T_lim.place(relx=0.6, rely=0.5+(i*height_items_basal), relwidth=0.4, relheight=height_items_basal)
		i = i+1
		IPI_text = Label(canvas, text="Intervalo IPI (ms):")
		IPI_text.place(relx=0, rely=0.5+(i*height_items_basal), relwidth=0.4, relheight=height_items_basal)
		IPI_entry = Entry(canvas, justify=CENTER, validate='key', vcmd=vcmd)
		IPI_entry.place(relx=0.4, rely=0.5+(i*height_items_basal), relwidth=0.3, relheight=height_items_basal)
		IPI_lim = Label(canvas, text="Limites: 1ms-64ms")
		IPI_lim.place(relx=0.6, rely=0.5+(i*height_items_basal), relwidth=0.4, relheight=height_items_basal)
		i = i+1
		ITI_text = Label(canvas, text="Intervalo ITI (s):")
		ITI_text.place(relx=0, rely=0.5+(i*height_items_basal), relwidth=0.4, relheight=height_items_basal)
		ITI_entry = Entry(canvas, justify=CENTER, validate='key', vcmd=vcmd)
		ITI_entry.place(relx=0.4, rely=0.5+(i*height_items_basal), relwidth=0.3, relheight=height_items_basal)
		ITI_lim = Label(canvas, text="Limites: 1s-4s")
		ITI_lim.place(relx=0.6, rely=0.5+(i*height_items_basal), relwidth=0.4, relheight=height_items_basal)
		i = i+1
		dur_text = Label(canvas, text="Duração (min):")
		dur_text.place(relx=0, rely=0.5+(i*height_items_basal), relwidth=0.4, relheight=height_items_basal)
		dur_entry = Entry(canvas, justify=CENTER, validate='key', vcmd=vcmd)
		dur_entry.place(relx=0.4, rely=0.5+(i*height_items_basal), relwidth=0.3, relheight=height_items_basal)
		dur_lim = Label(canvas, text="Limites: 1min - 30min")
		dur_lim.place(relx=0.6, rely=0.5+(i*height_items_basal), relwidth=0.4, relheight=height_items_basal)		
		i = i+1
		button_par_1 = Button(canvas, text="Selecionar parâmetros", command = lambda:select_par_basal(canvas, T_entry.get(), IPI_entry.get(), ITI_entry.get(), dur_entry.get()))	
		button_par_1.place(relx=0, rely=0.5+(i*height_items_basal), relwidth=1, relheight=height_items_basal)
		i = i+1	
		if (canvas==canvas1):
			global prot1
			prot1 = "Basal Unipolar"
		if (canvas==canvas2):
			global prot2
			prot2 = "Basal Unipolar"
		if (canvas==canvas3):
			global prot3
			prot3 = "Basal Unipolar"

	if (protocol=="Basal bipolar"):													
		prot_image = Image.open("protocolo_basal_bipolar.PNG")
		prot_photo = ImageTk.PhotoImage(prot_image.resize((420,110)))
		prot_label = Label(canvas, image=prot_photo)
		prot_label.image = prot_photo 
		prot_label.place(relx=0.0, rely=0.24, relwidth=1, relheight=0.275)		
		n_items_basal_bi = 6
		height_items_basal_bi = 0.5 / n_items_basal_bi
		i=0
		T_text = Label(canvas, text="Intervalo T (us):")
		T_text.place(relx=0, rely=0.5+(i*height_items_basal_bi), relwidth=0.4, relheight=height_items_basal_bi)
		T_entry = Entry(canvas, justify=CENTER, validate='key', vcmd=vcmd)
		T_entry.place(relx=0.4, rely=0.5+(i*height_items_basal_bi), relwidth=0.3, relheight=height_items_basal_bi)
		T_lim = Label(canvas, text="Limites: 5us-1000us")
		T_lim.place(relx=0.6, rely=0.5+(i*height_items_basal_bi), relwidth=0.4, relheight=height_items_basal_bi)
		i = i+1
		IPI_text = Label(canvas, text="Intervalo IPI (ms):")
		IPI_text.place(relx=0, rely=0.5+(i*height_items_basal_bi), relwidth=0.4, relheight=height_items_basal_bi)
		IPI_entry = Entry(canvas, justify=CENTER, validate='key', vcmd=vcmd)
		IPI_entry.place(relx=0.4, rely=0.5+(i*height_items_basal_bi), relwidth=0.3, relheight=height_items_basal_bi)
		IPI_lim = Label(canvas, text="Limites: 1ms-64ms")
		IPI_lim.place(relx=0.6, rely=0.5+(i*height_items_basal_bi), relwidth=0.4, relheight=height_items_basal_bi)
		i = i+1
		ITI_text = Label(canvas, text="Intervalo ITI (s):")
		ITI_text.place(relx=0, rely=0.5+(i*height_items_basal_bi), relwidth=0.4, relheight=height_items_basal_bi)
		ITI_entry = Entry(canvas, justify=CENTER, validate='key', vcmd=vcmd)
		ITI_entry.place(relx=0.4, rely=0.5+(i*height_items_basal_bi), relwidth=0.3, relheight=height_items_basal_bi)
		ITI_lim = Label(canvas, text="Limites: 1s-4s")
		ITI_lim.place(relx=0.6, rely=0.5+(i*height_items_basal_bi), relwidth=0.4, relheight=height_items_basal_bi)
		i = i+1
		dur_text = Label(canvas, text="Duração (min):")
		dur_text.place(relx=0, rely=0.5+(i*height_items_basal_bi), relwidth=0.4, relheight=height_items_basal_bi)
		dur_entry = Entry(canvas, justify=CENTER, validate='key', vcmd=vcmd)
		dur_entry.place(relx=0.4, rely=0.5+(i*height_items_basal_bi), relwidth=0.3, relheight=height_items_basal_bi)
		dur_lim = Label(canvas, text="Limites: 1min - 30min")
		dur_lim.place(relx=0.6, rely=0.5+(i*height_items_basal_bi), relwidth=0.4, relheight=height_items_basal_bi)		
		i = i+1
		button_par_1 = Button(canvas, text="Selecionar parâmetros", command = lambda:select_par_basal_bi(canvas, T_entry.get(), IPI_entry.get(), ITI_entry.get(), dur_entry.get()))	
		button_par_1.place(relx=0, rely=0.5+(i*height_items_basal_bi), relwidth=1, relheight=height_items_basal_bi)
		i = i+1	
		if (canvas==canvas1):
			
			prot1 = "Basal Bipolar"
		if (canvas==canvas2):
			
			prot2 = "Basal Bipolar"
		if (canvas==canvas3):
			
			prot3 = "Basal Bipolar"

	if (protocol=="TBS unipolar"):													
		prot_image = Image.open("protocolo_TBS.PNG")
		prot_photo = ImageTk.PhotoImage(prot_image.resize((450,130)))
		prot_label = Label(canvas, image=prot_photo)
		prot_label.image = prot_photo 
		prot_label.place(relx=0.0, rely=0.24, relwidth=1, relheight=0.275)		
		n_items_tbs = 9		
		height_items_tbs = 0.5 / n_items_tbs
		i=0
		T_text = Label(canvas, text="Intervalo T (ms):")
		T_text.place(relx=0, rely=0.5+(i*height_items_tbs), relwidth=0.4, relheight=height_items_tbs)
		T_entry = Entry(canvas, justify=CENTER, validate='key', vcmd=vcmd)
		T_entry.place(relx=0.4, rely=0.5+(i*height_items_tbs), relwidth=0.3, relheight=height_items_tbs)
		T_lim = Label(canvas, text="Limites: 1ms-100ms")
		T_lim.place(relx=0.6, rely=0.5+(i*height_items_tbs), relwidth=0.4, relheight=height_items_tbs)
		i=i+1
		IPI_text = Label(canvas, text="Intervalo IPI (ms):")
		IPI_text.place(relx=0, rely=0.5+(i*height_items_tbs), relwidth=0.4, relheight=height_items_tbs)
		IPI_entry = Entry(canvas, justify=CENTER, validate='key', vcmd=vcmd)
		IPI_entry.place(relx=0.4, rely=0.5+(i*height_items_tbs), relwidth=0.3, relheight=height_items_tbs)
		IPI_lim = Label(canvas, text="Limites: 1ms-100ms")
		IPI_lim.place(relx=0.6, rely=0.5+(i*height_items_tbs), relwidth=0.4, relheight=height_items_tbs)
		i=i+1
		n_ep_text = Label(canvas, text="Nº de epochs por trem:")
		n_ep_text.place(relx=0, rely=0.5+(i*height_items_tbs), relwidth=0.4, relheight=height_items_tbs)
		n_ep_entry = Entry(canvas, justify=CENTER, validate='key', vcmd=vcmd)
		n_ep_entry.place(relx=0.4, rely=0.5+(i*height_items_tbs), relwidth=0.3, relheight=height_items_tbs)
		n_ep_lim = Label(canvas, text="Limites: 1-20")
		n_ep_lim.place(relx=0.6, rely=0.5+(i*height_items_tbs), relwidth=0.4, relheight=height_items_tbs)
		i=i+1
		IEI_text = Label(canvas, text="Intervalo IEI (ms):")
		IEI_text.place(relx=0, rely=0.5+(i*height_items_tbs), relwidth=0.4, relheight=height_items_tbs)
		IEI_entry = Entry(canvas, justify=CENTER, validate='key', vcmd=vcmd)
		IEI_entry.place(relx=0.4, rely=0.5+(i*height_items_tbs), relwidth=0.3, relheight=height_items_tbs)
		IEI_lim = Label(canvas, text="Limites: 200ms-1000ms")
		IEI_lim.place(relx=0.6, rely=0.5+(i*height_items_tbs), relwidth=0.4, relheight=height_items_tbs)
		i=i+1
		ITI_text = Label(canvas, text="Intervalo ITI (s):")
		ITI_text.place(relx=0, rely=0.5+(i*height_items_tbs), relwidth=0.4, relheight=height_items_tbs)
		ITI_entry = Entry(canvas, justify=CENTER, validate='key', vcmd=vcmd)
		ITI_entry.place(relx=0.4, rely=0.5+(i*height_items_tbs), relwidth=0.3, relheight=height_items_tbs)
		ITI_lim = Label(canvas, text="Limites: 1s-20s")
		ITI_lim.place(relx=0.6, rely=0.5+(i*height_items_tbs), relwidth=0.4, relheight=height_items_tbs)
		i=i+1
		dur_text = Label(canvas, text="Duração (min):")
		dur_text.place(relx=0, rely=0.5+(i*height_items_tbs), relwidth=0.4, relheight=height_items_tbs)
		dur_entry = Entry(canvas, justify=CENTER, validate='key', vcmd=vcmd)
		dur_entry.place(relx=0.4, rely=0.5+(i*height_items_tbs), relwidth=0.3, relheight=height_items_tbs)
		dur_lim = Label(canvas, text="Limites: 1min - 30min")
		dur_lim.place(relx=0.6, rely=0.5+(i*height_items_tbs), relwidth=0.4, relheight=height_items_tbs)	
		i=i+1	
		button_par_1 = Button(canvas, text="Selecionar parâmetros", command = lambda:select_par_TBS(canvas, T_entry.get(), IPI_entry.get(), n_ep_entry.get(), IEI_entry.get(), ITI_entry.get(), dur_entry.get()))	
		button_par_1.place(relx=0, rely=0.5+(i*height_items_tbs), relwidth=1, relheight=height_items_tbs)
		i=i+1
		if (canvas==canvas1):
			prot1 = "TBS Unipolar"
		if (canvas==canvas2):
			prot2 = "TBS Unipolar"
		if (canvas==canvas3):
			prot3 = "TBS Unipolar"

	if (protocol=="TBS bipolar"):													
		prot_image = Image.open("protocolo_TBS_bipolar.PNG")
		prot_photo = ImageTk.PhotoImage(prot_image.resize((450,130)))
		prot_label = Label(canvas, image=prot_photo)
		prot_label.image = prot_photo 
		prot_label.place(relx=0.0, rely=0.24, relwidth=1, relheight=0.275)		
		n_items_tbs_bi = 9		
		height_items_tbs_bi = 0.5 / n_items_tbs_bi
		i=0
		T_text = Label(canvas, text="Intervalo T (ms):")
		T_text.place(relx=0, rely=0.5+(i*height_items_tbs_bi), relwidth=0.4, relheight=height_items_tbs_bi)
		T_entry = Entry(canvas, justify=CENTER, validate='key', vcmd=vcmd)
		T_entry.place(relx=0.4, rely=0.5+(i*height_items_tbs_bi), relwidth=0.3, relheight=height_items_tbs_bi)
		T_lim = Label(canvas, text="Limites: 1ms-100ms")
		T_lim.place(relx=0.6, rely=0.5+(i*height_items_tbs_bi), relwidth=0.4, relheight=height_items_tbs_bi)
		i=i+1
		IPI_text = Label(canvas, text="Intervalo IPI (ms):")
		IPI_text.place(relx=0, rely=0.5+(i*height_items_tbs_bi), relwidth=0.4, relheight=height_items_tbs_bi)
		IPI_entry = Entry(canvas, justify=CENTER, validate='key', vcmd=vcmd)
		IPI_entry.place(relx=0.4, rely=0.5+(i*height_items_tbs_bi), relwidth=0.3, relheight=height_items_tbs_bi)
		IPI_lim = Label(canvas, text="Limites: 1ms-100ms")
		IPI_lim.place(relx=0.6, rely=0.5+(i*height_items_tbs_bi), relwidth=0.4, relheight=height_items_tbs_bi)
		i=i+1
		n_ep_text = Label(canvas, text="Nº de epochs por trem:")
		n_ep_text.place(relx=0, rely=0.5+(i*height_items_tbs_bi), relwidth=0.4, relheight=height_items_tbs_bi)
		n_ep_entry = Entry(canvas, justify=CENTER, validate='key', vcmd=vcmd)
		n_ep_entry.place(relx=0.4, rely=0.5+(i*height_items_tbs_bi), relwidth=0.3, relheight=height_items_tbs_bi)
		n_ep_lim = Label(canvas, text="Limites: 1-20")
		n_ep_lim.place(relx=0.6, rely=0.5+(i*height_items_tbs_bi), relwidth=0.4, relheight=height_items_tbs_bi)
		i=i+1
		IEI_text = Label(canvas, text="Intervalo IEI (ms):")
		IEI_text.place(relx=0, rely=0.5+(i*height_items_tbs_bi), relwidth=0.4, relheight=height_items_tbs_bi)
		IEI_entry = Entry(canvas, justify=CENTER, validate='key', vcmd=vcmd)
		IEI_entry.place(relx=0.4, rely=0.5+(i*height_items_tbs_bi), relwidth=0.3, relheight=height_items_tbs_bi)
		IEI_lim = Label(canvas, text="Limites: 200ms-1000ms")
		IEI_lim.place(relx=0.6, rely=0.5+(i*height_items_tbs_bi), relwidth=0.4, relheight=height_items_tbs_bi)
		i=i+1
		ITI_text = Label(canvas, text="Intervalo ITI (s):")
		ITI_text.place(relx=0, rely=0.5+(i*height_items_tbs_bi), relwidth=0.4, relheight=height_items_tbs_bi)
		ITI_entry = Entry(canvas, justify=CENTER, validate='key', vcmd=vcmd)
		ITI_entry.place(relx=0.4, rely=0.5+(i*height_items_tbs_bi), relwidth=0.3, relheight=height_items_tbs_bi)
		ITI_lim = Label(canvas, text="Limites: 1s-20s")
		ITI_lim.place(relx=0.6, rely=0.5+(i*height_items_tbs_bi), relwidth=0.4, relheight=height_items_tbs_bi)
		i=i+1
		dur_text = Label(canvas, text="Duração (min):")
		dur_text.place(relx=0, rely=0.5+(i*height_items_tbs_bi), relwidth=0.4, relheight=height_items_tbs_bi)
		dur_entry = Entry(canvas, justify=CENTER, validate='key', vcmd=vcmd)
		dur_entry.place(relx=0.4, rely=0.5+(i*height_items_tbs_bi), relwidth=0.3, relheight=height_items_tbs_bi)
		dur_lim = Label(canvas, text="Limites: 1min - 30min")
		dur_lim.place(relx=0.6, rely=0.5+(i*height_items_tbs_bi), relwidth=0.4, relheight=height_items_tbs_bi)	
		i=i+1	
		button_par_1 = Button(canvas, text="Selecionar parâmetros", command = lambda:select_par_TBS_bi(canvas, T_entry.get(), IPI_entry.get(), n_ep_entry.get(), IEI_entry.get(), ITI_entry.get(), dur_entry.get()))	
		button_par_1.place(relx=0, rely=0.5+(i*height_items_tbs_bi), relwidth=1, relheight=height_items_tbs_bi)
		i=i+1
		if (canvas==canvas1):
			prot1 = "TBS Bipolar"
		if (canvas==canvas2):
			prot2 = "TBS Bipolar"
		if (canvas==canvas3):
			prot3 = "TBS Bipolar"

	if (protocol=="PBS"):													
		prot_image = Image.open("protocolo_PBS.PNG")
		prot_photo = ImageTk.PhotoImage(prot_image.resize((450,140)))
		prot_label = Label(canvas, image=prot_photo)
		prot_label.image = prot_photo 
		prot_label.place(relx=0.0, rely=0.24, relwidth=1, relheight=0.275)
		n_items_pbs = 8	
		height_items_pbs = 0.5 / n_items_pbs		
		i=0				
		PP_text = Label(canvas, text="Intervalo do pulso PP(us):")
		PP_text.place(relx=0, rely=0.5+(i*height_items_pbs), relwidth=0.4, relheight=height_items_pbs)		
		PP_entry = Entry(canvas, justify=CENTER, validate='key', vcmd=vcmd)
		PP_entry.place(relx=0.4, rely=0.5+(i*height_items_pbs), relwidth=0.3, relheight=height_items_pbs)
		PP_lim = Label(canvas, text="Limites: 5us-1000us")
		PP_lim.place(relx=0.6, rely=0.5+(i*height_items_pbs), relwidth=0.4, relheight=height_items_pbs)
		i=i+1
		PPI_text = Label(canvas, text="Intervalo PPI (ms):")
		PPI_text.place(relx=0, rely=0.5+(i*height_items_pbs), relwidth=0.4, relheight=height_items_pbs)
		PPI_entry = Entry(canvas, justify=CENTER, validate='key', vcmd=vcmd)
		PPI_entry.place(relx=0.4, rely=0.5+(i*height_items_pbs), relwidth=0.3, relheight=height_items_pbs)
		PPI_lim = Label(canvas, text="Limites: 1ms-64ms")
		PPI_lim.place(relx=0.6, rely=0.5+(i*height_items_pbs), relwidth=0.4, relheight=height_items_pbs)
		i=i+1
		T_text = Label(canvas, text="Intervalo T (us):")
		T_text.place(relx=0, rely=0.5+(i*height_items_pbs), relwidth=0.4, relheight=height_items_pbs)
		T_entry = Entry(canvas, justify=CENTER, validate='key', vcmd=vcmd)
		T_entry.place(relx=0.4, rely=0.5+(i*height_items_pbs), relwidth=0.3, relheight=height_items_pbs)
		T_lim = Label(canvas, text="Limites: 5us-1000us")
		T_lim.place(relx=0.6, rely=0.5+(i*height_items_pbs), relwidth=0.4, relheight=height_items_pbs)
		i=i+1
		IPI_text = Label(canvas, text="Intervalo IPI (us):")
		IPI_text.place(relx=0, rely=0.5+(i*height_items_pbs), relwidth=0.4, relheight=height_items_pbs)
		IPI_entry = Entry(canvas, justify=CENTER, validate='key', vcmd=vcmd)
		IPI_entry.place(relx=0.4, rely = 0.5+(i*height_items_pbs), relwidth=0.3, relheight=height_items_pbs)
		IPI_lim = Label(canvas, text="Limites: 5us-1000us")
		IPI_lim.place(relx=0.6, rely=0.5+(i*height_items_pbs), relwidth=0.4, relheight=height_items_pbs)
		i=i+1
		IBI_text = Label(canvas, text="Intervalo IBI (ms):")
		IBI_text.place(relx=0, rely=0.5+(i*height_items_pbs), relwidth=0.4, relheight=height_items_pbs)
		IBI_entry = Entry(canvas, justify=CENTER, validate='key', vcmd=vcmd)
		IBI_entry.place(relx=0.4, rely=0.5+(i*height_items_pbs), relwidth=0.3, relheight=height_items_pbs)
		IBI_lim = Label(canvas, text="Limites: 200ms-1000ms")
		IBI_lim.place(relx=0.6, rely=0.5+(i*height_items_pbs), relwidth=0.4, relheight=height_items_pbs)
		i=i+1
		dur_text = Label(canvas, text="Duração (min):")
		dur_text.place(relx=0, rely=0.5+(i*height_items_pbs), relwidth=0.4, relheight=height_items_pbs)
		dur_entry = Entry(canvas, justify=CENTER, validate='key', vcmd=vcmd)
		dur_entry.place(relx=0.4, rely = 0.5+(i*height_items_pbs), relwidth=0.3, relheight=height_items_pbs)
		dur_lim = Label(canvas, text="Limites: 1min - 30min")
		dur_lim.place(relx=0.6, rely = 0.5+(i*height_items_pbs), relwidth=0.4, relheight=height_items_pbs)	
		i=i+1	
		button_par_1 = Button(canvas, text="Selecionar parâmetros", command = lambda:select_par_PBS(canvas, PP_entry.get(), PPI_entry.get(), T_entry.get(), IPI_entry.get(), IBI_entry.get(), dur_entry.get()))	
		button_par_1.place(relx=0, rely = 0.5+(i*height_items_pbs), relwidth=1, relheight=height_items_pbs)	
		if (canvas==canvas1):
			prot1 = "PBS"
		if (canvas==canvas2):
			prot2 = "PBS"
		if (canvas==canvas3):
			prot3 = "PBS"

#Função para interface de seleção dos parâmetros de protocolo Basal unipolar
def select_par_basal(canvas, T, IPI, ITI, dur):
	try:
		T_f = int(T)	
		IPI_f = int(IPI)
		ITI_f = int(ITI)
		dur_f = int(dur)
		i=5
		n_items_basal = 6
		height_items_basal = 0.5 / n_items_basal
		n_train_calc = (60*dur_f)/((2*((T_f/1000000)+(IPI_f/1000)))+ITI_f)
		n_train_calc = int(n_train_calc)
		n_train_text = Label(canvas, text="Nº de trens do protocolo:")
		n_train_text.place(relx=0, rely=0.5+(i*height_items_basal), relwidth=0.5, relheight=height_items_basal)
		n_train_text_var = Label(canvas, text=n_train_calc)
		n_train_text_var.place(relx=0.5, rely=0.5+(i*height_items_basal), relwidth=0.5, relheight=height_items_basal)
		if (canvas==canvas1):
			global T_1
			global IPI_1
			global ITI_1
			global dur_1
			T_f += 256
			IPI_f += 256
			ITI_f += 256
			dur_f += 256
			T_1 = T_f.to_bytes(2,'little')
			IPI_1 = IPI_f.to_bytes(2,'little')
			ITI_1 = ITI_f.to_bytes(2,'little')
			dur_1 = dur_f.to_bytes(2,'little')
			print(T_1)
			print(IPI_1)
			print(ITI_1)
			print(dur_1)
			
			

		if (canvas==canvas2):
			global T_f_2
			global IPI_f_2
			global ITI_f_2
			global dur_f_2
			
		if (canvas==canvas3):
			global T_f_3
			global IPI_f_3
			global ITI_f_3
			global dur_f_3
					
		writeToLog("Parâmetros definidos.")
	except:
		writeToLog("Algum erro ocorreu. Tente preencher todos os parâmetros.")

#Função para interface de seleção dos parâmetros de protocolo Basal bipolar
def select_par_basal_bi(canvas, T, IPI, ITI, dur):
	try:
		T_f = int(T)	
		IPI_f = int(IPI)
		ITI_f = int(ITI)
		dur_f = int(dur)
		i=5
		n_items_basal_bi = 6
		height_items_basal_bi = 0.5 / n_items_basal_bi
		n_train_calc = (60*dur_f)/((2*((T_f/1000000)+(IPI_f/1000)))+ITI_f)
		n_train_calc = int(n_train_calc)
		n_train_text = Label(canvas, text="Nº de trens do protocolo:")
		n_train_text.place(relx=0, rely=0.5+(i*height_items_basal_bi), relwidth=0.5, relheight=height_items_basal_bi)
		n_train_text_var = Label(canvas, text=n_train_calc)
		n_train_text_var.place(relx=0.5, rely=0.5+(i*height_items_basal_bi), relwidth=0.5, relheight=height_items_basal_bi)
		if (canvas==canvas1):
			global T_1
			global IPI_1
			global ITI_1
			global dur_1
			

		if (canvas==canvas2):
			global T_f_2
			global IPI_f_2
			global ITI_f_2
			global dur_f_2
		
		if (canvas==canvas3):
			global T_f_3
			global IPI_f_3
			global ITI_f_3
			global dur_f_3
						
		writeToLog("Parâmetros definidos.")
	except:
		writeToLog("Algum erro ocorreu. Tente preencher todos os parâmetros.")
	

#Função para interface de seleção dos parâmetros de protocolo TBS unipolar
def select_par_TBS(canvas, T, IPI, n_ep, IEI, ITI, dur):
	try:
		T_f = int(T)
		IPI_f = int(IPI)
		n_ep_f = int(n_ep)
		IEI_f = int(IEI)
		ITI_f = int(ITI)
		dur_f = int(dur)
		i=7
		n_items_tbs = 9		
		height_items_tbs = 0.5 / n_items_tbs
		n_train_calc = (60*dur_f)/(((4*(T_f+IPI_f)/1000)+IEI_f/1000)*n_ep_f + ITI_f)
		n_train_calc = int(n_train_calc)
		n_train_text = Label(canvas, text="Nº de trens do protocolo:")
		n_train_text.place(relx=0, rely=0.5+(i*height_items_tbs), relwidth=0.5, relheight=height_items_tbs)
		n_train_text_var = Label(canvas, text=n_train_calc)
		n_train_text_var.place(relx=0.5, rely=0.5+(i*height_items_tbs), relwidth=0.5, relheight=height_items_tbs)
		i=i+1
		f_p_calc = 1000/(T_f+IPI_f)
		f_p_text = Label(canvas, text="Frequência dos pulsos (Hz):")
		f_p_text.place(relx=0, rely=0.5+(i*height_items_tbs), relwidth=0.5, relheight=height_items_tbs)
		f_p_text_var = Label(canvas, text=f_p_calc)
		f_p_text_var.place(relx=0.5, rely=0.5+(i*height_items_tbs), relwidth=0.5, relheight=height_items_tbs)
		if (canvas==canvas1):
			global T_1
			global IPI_1
			global n_ep_1
			global IEI_1
			global ITI_1
			global dur_1

		
		if (canvas==canvas2):
			global T_f_2
			global IPI_f_2
			global n_ep_f_2
			global IEI_f_2
			global ITI_f_2
			global dur_f_2
		
		if (canvas==canvas3):
			global T_f_3
			global IPI_f_3
			global n_ep_f_3
			global IEI_f_3
			global ITI_f_3
			global dur_f_3
			
		writeToLog("Parâmetros definidos.")
	except:
		writeToLog("Algum erro ocorreu. Tente preencher todos os parâmetros.")

#Função para interface de seleção dos parâmetros de protocolo TBS bipolar
def select_par_TBS_bi(canvas, T, IPI, n_ep, IEI, ITI, dur):
	try:
		T_f = int(T)
		IPI_f = int(IPI)
		n_ep_f = int(n_ep)
		IEI_f = int(IEI)
		ITI_f = int(ITI)
		dur_f = int(dur)
		i=7
		n_items_tbs_bi = 9		
		height_items_tbs_bi = 0.5 / n_items_tbs_bi
		n_train_calc = (60*dur_f)/(((4*(T_f+IPI_f)/1000)+IEI_f/1000)*n_ep_f + ITI_f)
		n_train_calc = int(n_train_calc)
		n_train_text = Label(canvas, text="Nº de trens do protocolo:")
		n_train_text.place(relx=0, rely=0.5+(i*height_items_tbs_bi), relwidth=0.5, relheight=height_items_tbs_bi)
		n_train_text_var = Label(canvas, text=n_train_calc)
		n_train_text_var.place(relx=0.5, rely=0.5+(i*height_items_tbs_bi), relwidth=0.5, relheight=height_items_tbs_bi)
		i=i+1
		f_p_calc = 1000/(T_f+IPI_f)
		f_p_text = Label(canvas, text="Frequência dos pulsos (Hz):")
		f_p_text.place(relx=0, rely=0.5+(i*height_items_tbs_bi), relwidth=0.5, relheight=height_items_tbs_bi)
		f_p_text_var = Label(canvas, text=f_p_calc)
		f_p_text_var.place(relx=0.5, rely=0.5+(i*height_items_tbs_bi), relwidth=0.5, relheight=height_items_tbs_bi)
		if (canvas==canvas1):
			global T_1
			global IPI_1
			global n_ep_1
			global IEI_1
			global ITI_1
			global dur_1
			
		if (canvas==canvas2):
			global T_f_2
			global IPI_f_2
			global n_ep_f_2
			global IEI_f_2
			global ITI_f_2
			global dur_f_2
			
		if (canvas==canvas3):
			global T_f_3
			global IPI_f_3
			global n_ep_f_3
			global IEI_f_3
			global ITI_f_3
			global dur_f_3
			
		writeToLog("Parâmetros definidos.")
	except:
		writeToLog("Algum erro ocorreu. Tente preencher todos os parâmetros.")

#Função para interface de seleção dos parâmetros de protocolo PBS
def select_par_PBS(canvas, PP, PPI, T, IPI, IBI, dur):
	try:
		PP_f = int(PP)
		PPI_f = int(PPI)
		T_f = int(T)
		IPI_f = int(IPI)
		IBI_f = int(IBI)
		dur_f = int(dur)
		i=6
		n_items_pbs = 8		
		height_items_pbs = 0.5 / n_items_pbs
		n_train_calc = (60*dur_f)/(((4*(T_f+IPI_f)/1000)+IBI_f/1000)*n_ep_f + ITI_f)
		n_train_calc = int(n_train_calc)
		n_train_text = Label(canvas, text="Nº de trens do protocolo:")
		n_train_text.place(relx=0, rely=0.5+(i*height_items_pbs), relwidth=0.5, relheight=height_items_pbs)
		n_train_text_var = Label(canvas, text=n_train_calc)
		n_train_text_var.place(relx=0.5, rely=0.5+(i*height_items_pbs), relwidth=0.5, relheight=height_items_pbs)
		f_p_calc = 1000/(T_f+IPI_f)
		f_p_text = Label(canvas, text="Frequência dos pulsos (Hz):")
		f_p_text.place(relx=0, rely=0.5+(i*height_items_pbs), relwidth=0.5, relheight=height_items_pbs)
		f_p_text_var = Label(canvas, text=f_p_calc)
		f_p_text_var.place(relx=0.5, rely=0.5+(i*height_items_pbs), relwidth=0.5, relheight=height_items_pbs)
		if (canvas==canvas1):
			global PP_f_1
			global PPI_f_1
			global T_f_1
			global IPI_f_1
			global IBI_f_1			
			global dur_f_1
			
		if (canvas==canvas2):
			global PP_f_2
			global PPI_f_2
			global T_f_2
			global IPI_f_2
			global IBI_f_2			
			global dur_f_2
			
		if (canvas==canvas3):
			global PP_f_3
			global PPI_f_3
			global T_f_3
			global IPI_f_3
			global IBI_f_3			
			global dur_f_3
			
		writeToLog("Parâmetros definidos.")
	except:
		writeToLog("Algum erro ocorreu. Tente preencher todos os parâmetros.")


def recvFromArduino():
	startMarker = 60
	endMarker = 62	
	ck = ""
	x = "z" # any value that is not an end- or startMarker
	byteCount = -1 # to allow for the fact that the last increment will be one too many	
	while  ord(x) != startMarker: 
		x = arduino.read()

	while ord(x) != endMarker:
		if ord(x) != startMarker:
			ck = ck + x.decode("utf-8") # change for Python3
			byteCount += 1
		x = arduino.read()    
	return(ck)


def waitForArduino():


	msg = ""
	while msg.find("Arduino is ready") == -1:

		while arduino.inWaiting() == 0:
			pass
        
		msg = recvFromArduino()
		print(msg)

#Função para detecção automática de Arduino e sua porta
def detect_arduino():
	ports = list(serial.tools.list_ports.comports())
	global Arduino_ports
	Arduino_ports=[]
	for p in ports:
		if 'Arduino' in p.description:
			Arduino_ports.append(p)

	if len(Arduino_ports)>=1:
		try:
			global arduino
			serPort = Arduino_ports[0].device
			baudRate = 9600
			arduino = serial.Serial(serPort, baudRate)
			print ("Serial port " + serPort + " opened  Baudrate " + str(baudRate))
			waitForArduino()
			writeToLog("Arduino detectado e conectado. PORTA: %s" %Arduino_ports[0].device)
			time.sleep(1)		

		except:
			writeToLog("Problema na conexão com Arduino. Reconectar USB.")	
	else:
		writeToLog("Não foi possível detectar Arduino conectado ao PC.")





#Função de envio de dados para Arduino
def send_protocol():		
		try:
			arduino.write(b'<')	
			if(prot1 == "Basal Unipolar"):
				arduino.write(b'B')
				arduino.write(b'U')
				arduino.write(T_1)
				arduino.write(IPI_1)
				arduino.write(ITI_1)
				arduino.write(dur_1)				
				arduino.write(b'0')
				arduino.write(b'0')
				arduino.write(b'0')
				arduino.write(b'0')
				arduino.write(b'>')	

			elif(prot1 == "TBS Unipolar"):
				arduino.write(b'T')
				arduino.write(b'U')	

			else:
				writeToLog("Problema no envio. Defina os protocolos desejados.")				
			
			
			receb = recvFromArduino()
			print(receb)			
			writeToLog("Protocolos enviados e iniciados com sucesso.")
		except: 
			writeToLog("Não foi possível enviar. Problema na conexão com Arduino.")

#Função de interrupção de protocolos
def interrupt_protocol():
	try:
		arduino.write(b'<')	
		arduino.write(b'I')	
		arduino.write(b'P')	
		arduino.write(b'>')	
		receb = recvFromArduino()
		print(receb)
		if(receb == "CR"):
			writeToLog("Protocolos interrompidos com sucesso.")	
	except: 
		writeToLog("Não foi possível enviar. Problema na conexão com Arduino.")

#Função de pular protocolo
def jump_protocol():
	try:
		arduino.write(b'<')	
		arduino.write(b'A')	
		arduino.write(b'P')	
		arduino.write(b'>')	
		receb = recvFromArduino()
		print(receb)
		if(receb == "CR"):
			writeToLog("Protocolo avançado com sucesso.")	
	except: 
		writeToLog("Não foi possível enviar. Problema na conexão com Arduino.")



root.mainloop()