﻿# Software para programação de protocolos em microcontrolador

Esse software tem como objetivo permitir ao usuário definir os protocolos desejados para estimulação assim como seus parâmetros e fazer com que os mesmos sejam produzidos pelo microcontrolador através da comunicação serial entre software e firmware. 

Para tanto, utiliza-se uma interface gráfica (GUI) produzida com Tkinter em Python para organização das variáveis de definição de protocolo a serem enviadas ao microcontrolador. A interface permite a seleção sequencial de até três protocolos de estimulação e seus respectivos parâmetros assim como a conexão e envio desses parâmetros ao Arduino. O envio das variáveis desejadas é feito através de uma string contendo as informações das variáveis e seus valores escolhidos. Além disso, a interface permite a interrupção da estimulação.

A interface gráfica em sua primeira versão é apresentada abaixo:

![Interface gráfica](/Imagens/interface_grafica.png) 


## Licença
A presente documentação descreve hardware, firmware e software de código aberto.
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licença Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />O trabalho <span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Neuroestimulador</span> de <a xmlns:cc="http://creativecommons.org/ns#" href="http://cta.if.ufrgs.br" property="cc:attributionName" rel="cc:attributionURL">Luís Eduardo Estradioto</a> está licenciado com uma Licença <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons - Atribuição-CompartilhaIgual 4.0 Internacional</a>.